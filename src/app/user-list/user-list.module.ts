import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user-list-routing.module';
import { UserListComponent } from './user-list.component';

import { MatTableModule, MatPaginatorModule } from '@angular/material'  
@NgModule({
  declarations: [UserListComponent],
  imports: [
    CommonModule,
    UserListRoutingModule,
    MatTableModule,
    MatPaginatorModule

    
  ]
})
export class UserListModule { }
