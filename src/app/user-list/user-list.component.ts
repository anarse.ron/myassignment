import { Component, OnInit, ViewChild } from '@angular/core';
import { userInterface } from './user.model';
import { Observable } from 'rxjs';
import { ApiService } from '../api/api.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { PageEvent } from '@angular/material';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  total = 0;
  total_pages;
  total_pagesdata : any;
  currentPage = 1;
  per_page = 6;
  userIsAuthenticated= false;  
  pageEvent: PageEvent;


  constructor(
      private http: HttpClient,
      private route: ActivatedRoute,
      private authservice : AuthService
  ) { }

  ngOnInit() {
      // load page based on 'page' query param or default to 1
      this.route.queryParams.subscribe(x => this.loadPage(x.page || 1));
      
  }

  private loadPage(page) {
      // get page of items from api
      this.http.get<any>(`https://reqres.in/api/users?page=${page}`).subscribe(x => {
          this.total_pagesdata = x.data;
          this.currentPage = x.page + 1;
          this.total = x.total
          console.log(this.total_pagesdata + "this is loaded data at first");
          console.log(this.currentPage + "current page number")
          console.log(this.total + "this is totaal page data")
      });
  }



  onPageclick(page){
      this.loadPage(page.pageIndex + 1);
  }
}


